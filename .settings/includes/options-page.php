<?php 
	global $commentbar;
	//saving the form
	if($_POST['comment_bar_save'] == 'y'){
		$commentbar->options->save_options($_POST['commentbar']);
	}
		
	$options = $commentbar->options->get_options();
		
?>

<style>
	div.commbentbar table td{
		padding-left: 20px;
	}
	
	div.commbentbar textarea{
		width: 500px;
		height: 100px;
	}
	
	div.commbentbar input{
		width: 500px;
		height: 30px;
	}
	
	div#commentbar_preview{
		height: 100px;
		width: 100%;
		<?php echo $options['css']; ?>		
	}
	
	
	
</style>


<div class="wrap">

	<h2>Comment Bar</h2>

	<form action="" method="post">
		<input type="hidden" name="comment_bar_save" value="y" />
	
		<div class="commbentbar styling">
			<table class="">
				<tr>
					<td><label for="commentbar_css"><h4>Css</h4></label></td>
					<td>
					<textarea name="commentbar[css]" id="commentbar_css"><?php echo $options['css']; ?></textarea>
					<p>For gradient Css, Please <a href="<?php echo $commentbar->options->get_gradient_url(); ?>" target="_blank" >click</a></p>
					</td>
				</tr>
			
				<tr>
					<td><label for="commentbar_autoresponder"><h4>Autoresponder</h4></label></td>
					<td><textarea name="commentbar[autoresponder]" id="commentbar_autoresponder"><?php echo $options['autoresponder']; ?></textarea></td>
				</tr>
				
				<tr>
					<td><label for="commentbar_required_text"><h4>Reqired Text</h4></label></td>
					<td><textarea name="commentbar[required_text]" id="commentbar_required_text"><?php echo $options['required_text']; ?></textarea></td>
				</tr>
			
				<tr>
					<td><label for="commentbar_thumbnail"><h4>Thumbnail</h4></label></td>
					<td><input <?php echo $options['thumbnail']; ?> type="text" name="commentbar[thumbnail]" id="commentbar_thumbnail"></td>
				</tr>
			
				<tr>
					<td><label for="commentbar_affiliate"><h4>Affiliate</h4></label></td>
					<td><input <?php echo $options['affiliate']; ?> type="text" name="commentbar[affiliate]" id="commentbar_affiliate"></td>
				</tr>
			</table>
		</div>
	
		<p>
			<input type="button" value="Preview" class="button button-secondary" /> &nbsp; &nbsp;
			<input type="submit" value="Save" class="button button-primary" />
		</p>	
		
	</form>
	
	<div id="commentbar_preview"></div>
	
</div>
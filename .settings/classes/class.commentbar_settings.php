<?php 
/*
 * Settings for both Post wise or Gerneral
 * */

class CommentBarSettings{
	
	private $options = array('css', 'autoresponder', 'required_text', 'affiliate');
	private $gradient_url = "http://www.colorzilla.com/gradient-editor/";
	
	function __construct(){
		add_action('admin_menu', array(&$this, 'admin_menu'));
	}
	
	
	//add option page
	function admin_menu(){
		add_options_page( 'Comment Bar Options', 'Comment Bar', 'manage_options', 'comment_bar', array(&$this, 'options_page'));
	}
	
	
	//options page content
	function options_page(){
		global $commentbar;
		include $commentbar->get_this_dir() . 'includes/options-page.php';
	}
	
	//save options
	function save_options($posted){
		$option_to_save = array();
		foreach($this->options as $option){
			if(isset($posted[$option])){
				$option_to_save[$option] = trim($posted[$option]);
			}
		}
		
		//var_dump($posted);
		//var_dump($option_to_save);
		
		update_option("commentbar_options", $option_to_save);
	}
	
	//get options
	function get_options(){
		$options = get_option("commentbar_options");
		return empty($options) ? array() : $options;
	}
	
	//get colore gredient url
	function get_gradient_url(){
		return $this->gradient_url;
	}
	
}


?>
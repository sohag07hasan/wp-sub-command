<?php 

/*
 * Plugin Name: Interactive Comment Bar
 * Author: Mahibul Hasan Sohag
 * Author uri: http://sohag07hasan.elance.com
 * */

define("COMMENTBAR_FILE", __FILE__);
define("COMMENTBAR_DIR", dirname(__FILE__) . '/');
define("COMMENTBAR_URI", plugins_url('/', __FILE__));

include COMMENTBAR_DIR . 'classes/comment-bar.php';
global $commentbar;
$commentbar = new CommentBar();


?>
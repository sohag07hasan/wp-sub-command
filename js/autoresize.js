var observe;
if (window.attachEvent) {
    observe = function (element, event, handler) {
        element.attachEvent('on'+event, handler);
    };
}
else {
    observe = function (element, event, handler) {
        element.addEventListener(event, handler, false);
    };
}
function init (id_selector) {
    var text = document.getElementById(id_selector);
    var holder =  document.getElementById('commentbar');
    function resize () {
        text.style.height = 'auto';
        text.style.height = text.scrollHeight+'px';
        
        holder.style.height = 'auto'; 
        holder.style.height = holder.scrollHeight+'px';
             
    }
    /* 0-timeout to get the already changed text */
    function delayedResize () {
        window.setTimeout(resize, 0);
    }
    observe(text, 'change',  resize);
    observe(text, 'cut',     delayedResize);
    observe(text, 'paste',   delayedResize);
    observe(text, 'drop',    delayedResize);
    observe(text, 'keydown', delayedResize);

    text.focus();
    text.select();
    resize();
}

//form validation
jQuery(document).ready(function($){	
	/* init('comment_bar_textarea'); */
	
	$('.comment_bar_form').submit(function(){
		var name = $(this).find('input[name=commenter_name]').val();
		var email = $(this).find('input[name=commenter_email]').val();
		var text = $(this).find('#comment_bar_textarea').val();
		
		var has_error = false;
		
		//name
		if(name == '' || name == null){
			$(this).find('input[name=commenter_name]').css({'border':'1px solid #FF0000' });
			has_error = true;
		}
		else{
			$(this).find('input[name=commenter_name]').css({'border':'none' });
		}
		
		if(!is_email(email)){
			$(this).find('input[name=commenter_email]').css({'border':'1px solid #FF0000' });
			has_error = true;
		}
		else{
			$(this).find('input[name=commenter_email]').css({'border':'none' });
		}
		
		//name
		if(text == '' || text == null){
			$(this).find('#comment_bar_textarea').css({'border':'1px solid #FF0000' });
			has_error = true;
		}
		else{
			$(this).find('#comment_bar_textarea').css({'border':'none' });
		}
		
		if(has_error){
			alert('Please check the highlighted field and try again');
			return false;
		}
	});
	
	
	var is_email = function(x){
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length){
		  return false;
		}
		else{
			return true;
		}
		
	};

});
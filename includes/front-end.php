<?php 

$options = $commentbar->options->get_options();

$time_delay = $commentbar->options->get_time_delay($post->ID);
$time_delay = ($time_delay > 0) ? $time_delay : 0;

$options['affiliate'] = strlen($options['affiliate']) > 5 ? $options['affiliate'] : $commentbar->options->get_default_affiliate();

$border = $commentbar->options->is_border_styling_enabled() ? '1px solid #' . $options['border'] : '';

$submit_button = '';
if($commentbar->options->is_submit_button_styler_enabled()){
	$submit_button .= 'background: #'. $options['submit_button'] . ' !important ; ';
	$submit_button .= 'color: #'. $options['submit_color'] . ' !important ;';
}

$style = '<style>
			
		div#commentbar{
			'.stripcslashes($options['css']).'
			border: '.$border.';
			color: #'.stripcslashes($options['text_color']).';		
		}
		
		input#commentbar_submit_button{
			cursor: pointer;
			cursor: hand;
			'.$submit_button.'
		}
		
		</style>';

$body = '<form class="comment_bar_form" action="" method="post" style="display: none;"> <input type="hidden" name="comment_bar_submitted" value="y" />';
$body .= '<input type="hidden" name="comment_post_ID" value="'.$post->ID.'">';
$body .= '
<div id="commentbar">
	
	<p class="heading-text">' . $options['required_text'] . '</p>'; 

if(strlen($options['thumbnail']) > 5){

	$body .= '<div class="comment_bar_thumb_container inside_commentbar"><img title="wp sub command" alt="" class="commbent_bar_thumb" src="' . $options['thumbnail'] . '" /> </div>';
}
else{
	$new_style = 'width: 90%;';
}
	
$body .=	'

	<div style="'.$new_style.'" class="comment_bar_required_text_container inside_commentbar">
		<p>Name: <input name="commenter_name" type="text" > Email: <input name="commenter_email" type="text"> </p>		
		<p>Comment:<br/> <textarea id="comment_bar_textarea" name="comment_text" rows="1" cols="50" class="comment-box"></textarea></p>
		<p class="ending-text">
			<span><input name="checked_for_newsletter" type="checkbox" checked /> Yes! I want to get gift </span>		 		
			<span class="submit-container"><input id="commentbar_submit_button" type="submit" value="Submit"> <a target="_blank" title="Wp Sub Command" href="'.$options['affiliate'].'"> Powered by WP Sub Command! </a> </span>
		</p>
	</div>
	<div style="clear:both"></div>
				
</div>';

$body .= '<form>';

$com = $style . $body;

$js = 
'<script>
	jQuery(document).ready(function(){
		var interval = '.$time_delay.';
		interval = interval * 1000;
		
		setTimeout(function(){
				jQuery(".comment_bar_form").show();
				init("comment_bar_textarea");
			}, 
		interval);
	});
</script>';

$com .= $js;
?>
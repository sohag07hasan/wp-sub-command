<?php 
	global $commentbar;
	//saving the form
	if($_POST['comment_bar_save'] == 'y'){
		$commentbar->options->save_options($_POST['commentbar']);
		
		if(isset($_POST['commentbar_enable_redirect'])){
			$commentbar->options->set_redirect_status('yes');
		}
		else{
			$commentbar->options->set_redirect_status('no');
		}
		
		if(isset($_POST['commentbar_enable_approve'])){
			$commentbar->options->set_auto_approve_status('yes');
		}
		else{
			$commentbar->options->set_auto_approve_status('no');
		}
		
		if(isset($_POST['commentbar_enable_border_styling'])){
			$commentbar->options->set_commentbar_border_status('yes');
		}
		else{
			$commentbar->options->set_commentbar_border_status('no');
		}
		
		if(isset($_POST['commentbar_enable_submit_styling'])){
			$commentbar->options->set_submit_button_styling_status('yes');
		}
		else{
			$commentbar->options->set_submit_button_styling_status('no');
		}
	}

	$redirect_status = $commentbar->options->is_redirect_enabled() ? 'yes' : 'no';
	$approve_status = $commentbar->options->is_auto_approved() ? 'yes' : 'no';
	$border_status = $commentbar->options->is_border_styling_enabled() ? 'yes' : 'no';
	$submit_status = $commentbar->options->is_submit_button_styler_enabled() ? 'yes' : 'no';	
	$options = $commentbar->options->get_options();
	
	
	//var_dump($options);
		
?>

<style>
	div.commbentbar table td{
		padding-left: 20px;
		padding-bottom: 10px;
	}	
	
	
	div.commbentbar textarea{
		width: 500px;
		height: 100px;
	}
	
	div.commbentbar input{
		width: 500px;
		height: 30px;
	}
	
	div#commentbar_preview{	
		width: 90%;
		height: 155px;
		overflow: hidden;
		<?php echo stripslashes($options['css']); ?>
		
		<?php if($commentbar->options->is_border_styling_enabled()) : ?>
		border: 1px solid #<?php echo stripslashes($options['border']); ?>;
		<?php endif; ?>
		
		color: 	#<?php echo stripslashes($options['text_color']); ?>;	
	}
	
		
	input#commentbar_submit_button{
		<?php if($commentbar->options->is_submit_button_styler_enabled()): ?>
		background-color: #<?php echo $options['submit_button']; ?>;
		color: #<?php echo $options['submit_color']; ?>;
		<?php endif; ?>
		
		cursor: pointer !important;
    	cursor: hand !important;
	}
	
	
		
</style>


<div class="wrap">

	<h2><?php echo $commentbar->plugin_name; ?></h2>

	<?php if($_POST['comment_bar_save'] == 'y'){ ?>
	<div class="updated"><p>Saved</p></div>
	<?php } ?>
	
	<form action="" method="post">
		<input type="hidden" name="comment_bar_save" value="y" />
	
		<div class="commbentbar styling">
			<table class="">
				<tr>
					<td><label for="commentbar_css"><h4>Css</h4></label></td>
					<td>
					<textarea name="commentbar[css]" id="commentbar_css"><?php echo stripslashes($options['css']); ?></textarea>
					<p>For gradient Css, Please <a href="<?php echo $commentbar->options->get_gradient_url(); ?>" target="_blank" >click</a></p>
					</td>
				</tr>
				
				<!-- 			
				<tr>
					<td><label for="commentbar_border"><h4> Border styling </h4></label></td>
					<td>
						<input class="color" type="text" name="commentbar[border]" id="commentbar_border" type="text" value="<?php echo stripslashes($options['border']); ?>" />
					</td>
				</tr>
				 -->
				
				<tr>
					<td><label for="commentbar_enable_border_styling"><h4>Enable Border styling? </h4></label></td>
					<td>
						<input <?php checked($border_status, 'yes'); ?> type="checkbox" name="commentbar_enable_border_styling" id="commentbar_enable_border_styling" value="yes" />
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_border"><h4> Border color </h4></label></td>
					<td>
						<input class="color" type="text" name="commentbar[border]" id="commentbar_border" type="text" value="<?php echo stripslashes($options['border']); ?>" />
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_text_color"><h4> Text Color </h4></label></td>
					<td>
						<input class="color" type="text" name="commentbar[text_color]" id="commentbar_commentbar_text_color" type="text" value="<?php echo stripslashes($options['text_color']); ?>" />
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_enable_submit_styling"><h4>Enable Submit Button Stylilng? </h4></label></td>
					<td>
						<input <?php checked($submit_status, 'yes'); ?> type="checkbox" name="commentbar_enable_submit_styling" id="commentbar_enable_submit_styling" value="yes" />
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_submit_button_color"><h4> Submit Button's Background </h4></label></td>
					<td>
						<input class="color" type="text" name="commentbar[submit_button]" id="commentbar_submit_button_color" type="text" value="<?php echo stripslashes($options['submit_button']); ?>" />
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_submit_color"><h4> Submit Button's Text Color </h4></label></td>
					<td>
						<input class="color" type="text" name="commentbar[submit_color]" id="commentbar_submit_color" type="text" value="<?php echo stripslashes($options['submit_color']); ?>" />
					</td>
				</tr>
			
				<tr>
					<td><label for="commentbar_autoresponder"><h4>Autoresponder Emails </h4></label></td>
					<td>
						<textarea name="commentbar[autoresponder]" id="commentbar_autoresponder"><?php echo stripslashes($options['autoresponder']); ?></textarea>
						<br/>Put one email per one line<br/>
					</td>
				</tr>
				
				<tr>
					<td><label for="commentbar_required_text"><h4>Required Text</h4></label></td>
					<td><textarea name="commentbar[required_text]" id="commentbar_required_text"><?php echo $options['required_text']; ?></textarea></td>
				</tr>
			
				<tr>
					<td><label for="commentbar_enable_redirect"><h4>Enable Redirect?</h4></label></td>
					<td><input <?php checked($redirect_status, 'yes'); ?> value="yes" type="checkbox" name="commentbar_enable_redirect" id="commentbar_enable_redirect"></td>
				</tr>
				
				<tr>
					<td><label for="commentbar_redirect_url"><h4>Redirect URL</h4></label></td>
					<td><input value="<?php echo $options['redirect_url']; ?>" type="text" name="commentbar[redirect_url]" id="commentbar_redirect_url"></td>
				</tr>
				
				<tr>
					<td><label for="commentbar_enable_approve"><h4>Approve Comment Automatically?</h4></label></td>
					<td><input <?php checked($approve_status, 'yes'); ?> value="yes" type="checkbox" name="commentbar_enable_approve" id="commentbar_enable_approve"></td>
				</tr>
			
				<tr>
					<td><label for="commentbar_thumbnail"><h4>Thumbnail</h4></label></td>
					<td class="upload">
						<input value="<?php echo $options['thumbnail']; ?>" type="text" name="commentbar[thumbnail]" class="commentbar_thumbnail"> 
						 <input style="width: 130px;" type='button' class='button button-secondary button-upload' value='Upload an image'/></br>
						<img style='max-width: 150px; max-height: 150px; display:block' src='<?php echo $options['thumbnail']; ?>' class='preview-upload'/>
					</td>
				</tr>
			
				<tr>
					<td><label for="commentbar_affiliate"><h4>Affiliate</h4></label></td>
					<td><input value="<?php echo $options['affiliate']; ?>" type="text" name="commentbar[affiliate]" id="commentbar_affiliate"></td>
				</tr>
								
				
			</table>
		</div>
	
		<p>
			<input type="submit" value="Preview" class="button button-primary" />
		</p>	
		
	</form>
	
	<!--  cooment bar preview -->
	<!-- <div class="commentbar_preview_holder"> -->
		<div id="commentbar_preview">
			<div class="comment_bar_thumb_container inside_commentbar"> 
				<img class="commbent_bar_thumb" src="<?php echo $options['thumbnail'];  ?>" />
			</div>
			<div class="comment_bar_required_text_container inside_commentbar">
				<p><?php echo $options['required_text']; ?></p>
				<p>Name: <input type="text" >, Email: <input type="text"> <span>Yes! I want to get gift <input type="checkbox" checked /></span></p>
				
				<p>Comment: <textarea rows="1" cols="50" class="comment-box"></textarea>&nbsp; 		
				<input id="commentbar_submit_button" type="submit" value="Submit"> <a href="<?php echo $options['affiliate'] ?>"> Powered by WP Sub Command! </a></p>
			</div>
		</div>
	<!-- </div>  -->
	
</div>
<?php 
	global $commentbar, $post;
	$is_active = $commentbar->options->is_commentbar_active($post->ID);
	
	$checked = $is_active ? "1" : "0";
	$time_delay = $commentbar->options->get_time_delay($post->ID);
?>

<div class="commentbar post-page">
	<p><input <?php checked("1", $checked); ?> type="checkbox" id="commentbar_activate" name="commentbar_activate" value="1" /> <label for="commentbar_activate">Activate Comment Bar</label> </p>
	<p> <label for="commentbar_timedelay">Time Delay (seconds)</label> <br/> <input type="text" id='commentbar_timedelay' name="commentbar_timedealy" value="<?php echo $time_delay; ?>"  /> </p> 
</div>
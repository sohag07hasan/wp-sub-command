<?php 

class CommentBarFrontEnd{
	
	function __construct(){
		//add_action('wp_footer', array(&$this, 'include_comment_bar'));
		add_action('wp_enqueue_scripts', array(&$this, 'include_scripts'));
		add_filter('the_content', array(&$this, 'add_comment_bar'));
		
		//form submission
		add_action('init', array(&$this, 'comment_bar_submitted'));
	}
	
	
	//include the comment bar in footer sectio n
	function include_comment_bar(){
		if(is_page() || is_single()):
			global $commentbar, $post;
			if($commentbar->options->is_commentbar_active($post->ID)){
				include $commentbar->get_this_dir() . 'includes/front-end.php';
			}
		endif;
	}
	
	
	//include scripts
	function include_scripts(){
		if(is_page() || is_single()):
			global $commentbar, $post;
			if($commentbar->options->is_commentbar_active($post->ID)){
				wp_register_style('commentbar-front-end-css', $commentbar->get_this_url() . 'css/front-end.css');
				wp_enqueue_style('commentbar-front-end-css');
				
				//javascript
				wp_register_script('comment-bar-auto-resize', $commentbar->get_this_url() . 'js/autoresize.js', array('jquery'));
				wp_enqueue_script('comment-bar-auto-resize');
			}
		endif;
	}
	
	//add comment bar
	function add_comment_bar($content){
		if(is_page() || is_single()){
			global $commentbar, $post;	
			if($commentbar->options->is_commentbar_active($post->ID)){		
				include $commentbar->get_this_dir() . 'includes/front-end.php';			
				$content .= $com;
			}
		}
		
		return $content;
	}
	
	
	//comment bar submitted
	function comment_bar_submitted(){
		global $commentbar;
		if($_POST['comment_bar_submitted'] == 'y'){
			$args = array();
			$args['comment_post_ID'] = (int) $_POST['comment_post_ID'];
			$args['comment_author'] = $_POST['commenter_name'];
			$args['comment_author_email'] = $_POST['commenter_email'];
			$args['comment_content'] = $_POST['comment_text'];
			$args['comment_author_IP'] = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ;
			$args['comment_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$args['comment_approved'] = $commentbar->options->is_auto_approved() ? 1 : 0;			
			
			$comment_id = wp_insert_comment($args);
			
			//options
			$options = $commentbar->options->get_options();
			
			if(isset($_POST['checked_for_newsletter'])){
				//now autoresponders emails
				$emails = $this->extract_emails($options['autoresponder']);
				
				
				
				if(!empty($emails)){
					$subject = "Newsletter";
					$body = 'autosubscriber';
					$headers[] = 'From: '.$args['comment_author'].' <'.$args['comment_author_email'].'>' . "\r\n";
					
					$commentbar->send_email($emails, $subject, $body, $headers);
				}
			}
			
			//now redirection
			if($commentbar->options->is_redirect_enabled()){				
				$redirect_url = $options['redirect_url'];				
				if($redirect_url){
					return $commentbar->redirect($redirect_url);
				}
			}
			
		}
	}
	
	
	
	//extract emails
	function extract_emails($string){
		$emails = array();
		
		$e = explode("\n", $string);
		if($e){
			foreach ($e as $email){
				$email = trim($email);
				if(is_email($email)){
					$emails[] = $email;
				}
			}
		}
		
		return $emails;
	}
	
}

?>

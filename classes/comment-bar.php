<?php 
/*
 * This class is the controller of the every class
 * */

class CommentBar{
	
	public $options;
	public $postview;
	public $plugin_name;
		
	//constructor
	function __construct(){
		include $this->get_this_dir() . 'classes/class.commentbar_settings.php';
		$this->options = new CommentBarSettings();
		
		include $this->get_this_dir() . 'classes/class.post-view.php';
		$this->postview = new CommentBarFrontEnd();
		
		$this->plugin_name = "WP Sub Command";
	}
	
	//get base directory of the current plugin
	function get_this_dir(){
		return COMMENTBAR_DIR;
	}

	//get current plugin's base url
	function get_this_url(){
		return COMMENTBAR_URI;
	}
	
	
	//redirect
	function redirect($url){
		if(!function_exists('wp_redirect')){
			include ABSPATH . 'wp-includes/pluggable.php';
		}
		
		wp_redirect($url);
		die();
	}
	
	
	//email sending
	function send_email($to, $subject, $content, $headers = array()){
		if(!function_exists('wp_mail')){
			include ABSPATH . 'wp-includes/pluggable.php';
		}
		if(!is_array($to)){
			$to = array($to);
		}
		
		foreach($to as $t){
			return wp_mail($t, $subject, $content, $headers);
		}
	}
	
}

?>
<?php 
/*
 * Settings for both Post wise or Gerneral
 * */

class CommentBarSettings{
	
	private $options = array('border','text_color', 'css', 'autoresponder', 'required_text', 'affiliate', 'thumbnail', 'redirect_url', 'submit_button', 'submit_color');
	private $gradient_url = "http://www.colorzilla.com/gradient-editor/";
	private $default_affiliate = 'http://wp-subcommand.com';
	
	function __construct(){
		add_action('admin_menu', array(&$this, 'admin_menu'));
		
		//add metaboxex
		add_action( 'add_meta_boxes', array(&$this, 'add_meta_boxes' ));
		
		//save meta box info
		add_action('save_post', array(&$this, 'save_meta_boxes'), 10, 2);
		
		//necessary scripts for default media uploader
		add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
	}
	
	
	//add option page
	function admin_menu(){
		global $commentbar;
		add_options_page( "$commentbar->plugin_name | Options", $commentbar->plugin_name, 'manage_options', 'comment_bar', array(&$this, 'options_page'));
	}
	
	
	//options page content
	function options_page(){
		global $commentbar;
		include $commentbar->get_this_dir() . 'includes/options-page.php';
	}
	
	//save options
	function save_options($posted){
		$option_to_save = array();
		foreach($this->options as $option){
			if(isset($posted[$option])){
				$option_to_save[$option] = trim($posted[$option]);
			}
		}
		
		//var_dump($posted);
		//var_dump($option_to_save);
		
		update_option("commentbar_options", $option_to_save);
	}
	
	//get options
	function get_options(){
		$options = get_option("commentbar_options");
		return empty($options) ? array() : $options;
	}
	
	//get colore gredient url
	function get_gradient_url(){
		return $this->gradient_url;
	}
	
	//add some metaboxes
	function add_meta_boxes(){
		$post_types = array('post', 'page');
		foreach($post_types as $post_type){
			add_meta_box( 'Wp Sub Command | Post Options', __( 'Comment Bar', 'commentbar' ), array(&$this, 'commentbar_options_for_posts'), $post_type, 'side', 'high' );
		}
	}
	
	//comment bar option for page
	function commentbar_options_for_posts($post){
		global $commentbar;
		include $commentbar->get_this_dir() . 'includes/metaboxes/per-page.php';
	}
	
	//svae the meta boxes
	function save_meta_boxes($post_id, $post){
		
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
		
		if(isset($_POST['commentbar_activate'])){
			update_post_meta($post_id, '_commentbar_activate', "1");
		}
		else{
			update_post_meta($post_id, '_commentbar_activate', "0");
		}
		
		if(isset($_POST['commentbar_timedealy'])){
			update_post_meta($post_id, '_commentbar_timedealy', $_POST['commentbar_timedealy']);
		}		
	}
	
	//boolean to check if comment bar is active
	function is_commentbar_active($post_id){
		return (get_post_meta($post_id, '_commentbar_activate', true) == "1") ? true : false;  
	}
	
	
	//return the time delay in seconds when the comment bar pops up
	function get_time_delay($post_id){
		$default = 30;
		$delay = get_post_meta($post_id, '_commentbar_timedealy', true);
		
		return empty($delay) ? $default : (int) $delay;
	}
	
	
	//scripts to use default media uploader
	function admin_enqueue_scripts(){
		global $commentbar;
		
		if($_GET['page'] == 'comment_bar'){
			wp_enqueue_script('jquery');
			wp_enqueue_style( 'thickbox' ); // Stylesheet used by Thickbox
			wp_enqueue_script( 'thickbox' );
			wp_enqueue_script( 'media-upload' );
			wp_register_script('commentbar-media-upload', $commentbar->get_this_url() . 'js/plugin.media-uploader.js', array('jquery', 'thickbox', 'media-upload'));
			wp_enqueue_script('commentbar-media-upload');
						
			wp_register_style('comment-bar-preview-styling', $commentbar->get_this_url() . 'css/comment-bar-preview.css');
			wp_enqueue_style('comment-bar-preview-styling');
			
			//wp_register_style('commentbar-front-end-css', $commentbar->get_this_url() . 'css/front-end.css');
			//wp_enqueue_style('commentbar-front-end-css');
			
			//color picker
			wp_register_script('commentbar-js-color-picker', $commentbar->get_this_url() . 'js/jscolor/jscolor.js');
			wp_enqueue_script('commentbar-js-color-picker');
			
		}
	}
	
	
	//set redirect status
	function set_redirect_status($status){
		update_option('comment_bar_redirect_enabled', $status);
	}
	
	
	//redirect enabled?
	function is_redirect_enabled(){
		$status = get_option('comment_bar_redirect_enabled');
		return $status == 'yes' ? true : false;
	}
	
	
	//set comment approve status
	function set_auto_approve_status($status){
		update_option('comment_bar_auto_approve', $status);
	}
	
	
	//boolean to check the approval status
	function is_auto_approved(){
		$status = get_option('comment_bar_auto_approve');
		return $status == 'yes' ? true : false;
	}
	
	
	//comment bar border stylilng status
	function set_commentbar_border_status($status){
		update_option('commentbar_border_enabled', $status);
	}
	
	//boolean to check if border syling is enalbed
	function is_border_styling_enabled(){
		$status = get_option('commentbar_border_enabled');
		return $status == 'yes' ? true : false;
	}
	
	
	//submit button styling
	function set_submit_button_styling_status($status){
		update_option('commentbar_submit_button_styling', $status);
	}
	
	//boolean to check if submit button styling is enabled
	function is_submit_button_styler_enabled(){
		$status = get_option('commentbar_submit_button_styling');
		return $status == 'yes' ? true : false;
	}
	
	//get affiliate
	function get_default_affiliate(){
		return $this->default_affiliate;
	}
}


?>